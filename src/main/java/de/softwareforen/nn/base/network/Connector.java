/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.softwareforen.nn.base.network;

import de.softwareforen.nn.base.network.neuron.Neuron;
import de.softwareforen.nn.base.Util;

/**
 *
 * @author wolframl
 */
public class Connector {
	private void connectNe(Neuron source, Neuron destination, Float weight) {
		destination.setWeight(source, weight);
		source.getOutputConnections().add(destination);
	}

	private void connectNe(Neuron source, Neuron destination) {
		connectNe(source, destination, Util.rand());
	}

	private void connectLayers(Layer layer1, Layer layer2)
    {
        layer1.stream().forEach((inputNe) ->
        {
            layer2.stream().forEach((targetNe) ->
            {
                connectNe(inputNe, targetNe);
            });
        });
    }

	public void connectLayers(Network network) {
		connectLayers(network.getInputLayer(), network.getHiddenLayer(0));
		for (int i = 1; i < network.getHiddenLayersCount(); i++) {
			connectLayers(network.getHiddenLayer(i - 1),
					network.getHiddenLayer(i));
		}
		connectLayers(
				network.getHiddenLayer(network.getHiddenLayersCount() - 1),
				network.getOutputLayer());
	}
}
