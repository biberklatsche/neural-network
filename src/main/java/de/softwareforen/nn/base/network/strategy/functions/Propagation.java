package de.softwareforen.nn.base.network.strategy.functions;

import de.softwareforen.nn.base.network.neuron.Neuron;

/**
 *
 * @author wolframl
 */
public class Propagation {

	public static PropagationFunction sumFunction() {
        return (inputConnectionToWeights) 
            -> {
            Float value = 0f;
            for (Neuron inputNeuron : inputConnectionToWeights.keySet()) {
                value += inputNeuron.getOutput()
                    * inputConnectionToWeights.get(inputNeuron);
            }
            return value;

        };
    }
}
