package de.softwareforen.nn.base.network;

import de.softwareforen.nn.base.network.neuron.Neuron;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Layer implements List<Neuron> {
	private final List<Neuron> neurons;

    Layer(int countNeurons){
        neurons = new ArrayList(countNeurons);
    }
    
    @Override
    public boolean add(Neuron neuron)
    {
        return neurons.add(neuron);
    }

    @Override
    public void add(int index, Neuron neuron)
    {
        neurons.add(index, neuron);
    }

    @Override
    public boolean addAll(
        Collection<? extends Neuron> neurons)
    {
        return this.neurons.addAll(neurons);
    }

    @Override
    public boolean addAll(int index,
        Collection<? extends Neuron> c)
    {
        return this.neurons.addAll(index, neurons);
    }

    @Override
    public void clear()
    {
        neurons.clear();
    }

    @Override
    public boolean contains(Object o)
    {
        return neurons.contains(o);
    }

    @Override
    public boolean containsAll(
        Collection<?> c)
    {
        return neurons.containsAll(c);
    }

    @Override
    public Neuron get(int index)
    {
        return neurons.get(index);
    }

    @Override
    public int indexOf(Object o)
    {
        return neurons.indexOf(o);
    }

    @Override
    public boolean isEmpty()
    {
        return neurons.isEmpty();
    }

    @Override
    public int lastIndexOf(Object o)
    {
        return neurons.lastIndexOf(o);
    }

    @Override
    public ListIterator<Neuron> listIterator()
    {
        return neurons.listIterator();
    }

    @Override
    public ListIterator<Neuron> listIterator(int index)
    {
        return neurons.listIterator(index);
    }

    @Override
    public boolean remove(Object o)
    {
        return neurons.remove(o);
    }

    @Override
    public Neuron remove(int index)
    {
        return neurons.remove(index);
    }

    @Override
    public boolean removeAll(
        Collection<?> c)
    {
        return neurons.removeAll(c);
    }

    @Override
    public boolean retainAll(
        Collection<?> c)
    {
        return neurons.retainAll(c);
    }

    @Override
    public Neuron set(int index, Neuron element)
    {
        return neurons.set(index, element);
    }

    @Override
	public int size() {
		return neurons.size();
	}

    @Override
    public List<Neuron> subList(int fromIndex, int toIndex)
    {
        return neurons.subList(fromIndex, toIndex);
    }

    @Override
    public Object[] toArray()
    {
        return neurons.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a)
    {
        return neurons.toArray(a);
    }

	@Override
	public Iterator iterator() {
		return neurons.iterator();
	}
}
