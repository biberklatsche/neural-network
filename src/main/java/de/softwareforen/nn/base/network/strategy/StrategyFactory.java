package de.softwareforen.nn.base.network.strategy;

import de.softwareforen.nn.base.network.neuron.Neuron;
import de.softwareforen.nn.base.network.strategy.functions.ActivationDerivation;
import de.softwareforen.nn.base.network.strategy.functions.ActivationFunction;
import de.softwareforen.nn.base.network.strategy.functions.BiasFunction;
import de.softwareforen.nn.base.network.strategy.functions.PropagationFunction;
import de.softwareforen.nn.exceptions.InitialisationException;
import java.util.Map;
import java.util.function.Function;

public class StrategyFactory {
	private Strategy s;

	private StrategyFactory() {
		s = new Strategy();
	};

	public static StrategyFactory create() {
		return new StrategyFactory();
	}

	public StrategyFactory with(ActivationFunction function) {
		this.s.setActivationFunction(function);
		return this;
	}

	public StrategyFactory with(
			ActivationDerivation function) {
		this.s.setActivationDerivation(function);
		return this;
	}

	public StrategyFactory with(BiasFunction function) {
		this.s.setBiasFunction(function);
		return this;
	}

	public StrategyFactory with(PropagationFunction function) {
		this.s.setPropagationFunction(function);
		return this;
	}

	public Strategy build() throws InitialisationException {
		validate();
		return s;
	}

	private void validate() throws InitialisationException {
		if (s.getBiasFunction() == null) {
			throw new InitialisationException(
					"Es muss eine Biasfunktion angegeben werden.");
		}
		if (s.getPropagationFunction() == null) {
			throw new InitialisationException(
					"Es muss ein Propagierungsfunktion angegeben werden.");
		}
		if (s.getActivationDerivation() == null) {
			throw new InitialisationException(
					"Es muss eine Ableitung der Aktivierungsfunktion angegeben werden.");
		}
		if (s.getActivationFunction() == null) {
			throw new InitialisationException(
					"Es muss eine Aktivierungsfunktion angegeben werden.");
		}
	}
}
