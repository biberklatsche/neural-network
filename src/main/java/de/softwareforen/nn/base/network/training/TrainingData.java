package de.softwareforen.nn.base.network.training;

import java.util.List;

public class TrainingData {
	private final List<Float> inputs;
	private final List<Float> outputs;

	public TrainingData(List<Float> inputs, List<Float> outputs) {
		this.inputs = inputs;
		this.outputs = outputs;
	}

	/**
	 * @return the inputs
	 */
	public List<Float> getInputs() {
		return inputs;
	}

	/**
	 * @return the outputs
	 */
	public List<Float> getOutputs() {
		return outputs;
	}
}
