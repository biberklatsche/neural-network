package de.softwareforen.nn.base.network;

import de.softwareforen.nn.base.network.neuron.NeuronImpl;
import de.softwareforen.nn.base.network.strategy.Strategy;
import de.softwareforen.nn.exceptions.InitialisationException;

public class NetworkFactory {

	private Strategy strategy;
	private Connector connector = new Connector();
	private int[] neuronsPerLayer;

	private NetworkFactory() {
	};

	public static NetworkFactory create() {
		return new NetworkFactory();
	}

	public NetworkFactory with(int... neuronsPerLayer) {
		this.neuronsPerLayer = neuronsPerLayer;
		return this;
	}

	public NetworkFactory with(Strategy strategy) {
		this.strategy = strategy;
		return this;
	}

	public NetworkFactory with(Connector connector) {
		this.connector = connector;
		return this;
	}

	public Network build() throws InitialisationException {
		validate();
		Network network = new Network(neuronsPerLayer.length - 2);
		Layer inputLayer = new Layer(neuronsPerLayer[0]);
		for (int i = 0; i < neuronsPerLayer[0]; i++) {
			inputLayer.add(new NeuronImpl(strategy));
		}
		network.setInputLayer(inputLayer);
		for (int i = 1; i < neuronsPerLayer.length - 1; i++) {
			int neuronsPerHiddenLayer = neuronsPerLayer[i];
			Layer layer = new Layer(neuronsPerHiddenLayer);
			for (int j = 0; j < neuronsPerHiddenLayer; j++) {
				layer.add(new NeuronImpl(strategy));
			}
			network.addHiddenLayer(layer);
		}
		Layer outputLayer = new Layer(
				neuronsPerLayer[neuronsPerLayer.length - 1]);
		for (int i = 0; i < neuronsPerLayer[neuronsPerLayer.length - 1]; i++) {
			outputLayer.add(new NeuronImpl(strategy));
		}
		network.setOutputLayer(outputLayer);
		connector.connectLayers(network);
		return network;
	}

	private void validate() throws InitialisationException {
		if (strategy == null) {
			throw new InitialisationException(
					"Es muss eine Strategie angegeben werden.");
		}
		if (connector == null) {
			throw new InitialisationException(
					"Es muss ein Connector angegeben werden.");
		}
		if (neuronsPerLayer == null || neuronsPerLayer.length < 2) {
			throw new InitialisationException(
					"Es müssen mindestens ein Input- und ein Output-Layer existieren.");
		}
	}
}
