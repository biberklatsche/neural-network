package de.softwareforen.nn.base.network.strategy.functions;

import java.util.function.Function;

public interface ActivationDerivation extends Function<Float, Float> {

}
