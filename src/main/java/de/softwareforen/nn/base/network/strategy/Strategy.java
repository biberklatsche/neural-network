package de.softwareforen.nn.base.network.strategy;

import de.softwareforen.nn.base.network.neuron.Neuron;
import de.softwareforen.nn.base.network.training.Constants;
import java.util.Map;
import java.util.function.Function;

public class Strategy {

	private Function<Map<Neuron, Float>, Float> propagationFunction;

	private Function<Float, Float> activationFunction;

	private Function<Float, Float> activationDerivation;

	private Function<Float, Float> biasFunction;

	public Float propagation(Map<Neuron, Float> inputConnectionToWeights,
			Float bias) {
		return getPropagationFunction().apply(inputConnectionToWeights) + bias;
	}

	public Float activation(Float propagation) {
		return getActivationFunction().apply(propagation);
	}

	public Float findDelta(Float outputValue, Float errorFactor) {
		return getActivationDerivation().apply(outputValue) * errorFactor;
	}

	public Float findNewBias(Float bias, Float deltaValue) {
		return bias + getBiasFunction().apply(deltaValue);
	}

	public Float findNewWeight(Float currentWeight, Float outputValue,
			Float deltaValue) {
		return currentWeight + Constants.LEARNING_RATE * outputValue
				* deltaValue;
	}

	public void setPropagationFunction(
			Function<Map<Neuron, Float>, Float> function) {
		this.propagationFunction = function;
	}

	public void setActivationFunction(Function<Float, Float> function) {
		this.activationFunction = function;
	}

	public void setActivationDerivation(Function<Float, Float> function) {
		this.activationDerivation = function;
	}

	public void setBiasFunction(Function<Float, Float> function) {
		this.biasFunction = function;
	}

	public Function<Map<Neuron, Float>, Float> getPropagationFunction() {
		return propagationFunction;
	}

	public Function<Float, Float> getActivationFunction() {
		return activationFunction;
	}

	public Function<Float, Float> getActivationDerivation() {
		return activationDerivation;
	}

	public Function<Float, Float> getBiasFunction() {
		return biasFunction;
	}

}
