package de.softwareforen.nn.base.network.strategy.functions;

/**
 *
 * @author wolframl
 */
public class Activation {

	public static ActivationFunction sigmoidFunction() {
        return (value) -> {
            float f = (float) Math.exp(-value);
            return 1 / (1 + f);
        };
    }

	public static ActivationDerivation sigmoidDerivation() {
        return (value) -> {
            return value * (1 - value);
        };
	}
}
