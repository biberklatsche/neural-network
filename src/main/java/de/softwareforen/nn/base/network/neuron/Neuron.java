package de.softwareforen.nn.base.network.neuron;

import java.util.List;

public interface Neuron {
	public Float getBias();

	public Float getOutput();

	public void setOutput(Float output);

	public Float getDeltaValue();

	public List<Neuron> getOutputConnections();

	public Float getWeight(Neuron inputNeuron);

	public void setWeight(Neuron inputNeuron, Float weight);

	public void updateOutput();

	public void updateDelta(Float errorFactor);

	public void updateWeight();

	public void updateBias();
}
