package de.softwareforen.nn.base.network.neuron;

import de.softwareforen.nn.base.network.strategy.Strategy;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class NeuronImpl implements Neuron {

	private Float biasValue = -1f;

	private Float outputValue = 0f;

	private Float deltaValue = 0f;

	private final Map<Neuron, Float> inputConnectionsToWeight = new HashMap();

	private final List<Neuron> outputConnections = new LinkedList();

	private final Strategy strategy;

	public NeuronImpl(Strategy strategy) {
		this.strategy = strategy;
	}

	@Override
	public Float getBias() {
		return biasValue;
	}

	@Override
	public Float getOutput() {
		return outputValue;
	}

	@Override
	public Float getDeltaValue() {
		return deltaValue;
	}

	@Override
	public List<Neuron> getOutputConnections() {
		return outputConnections;
	}

	@Override
	public Float getWeight(Neuron inputNeuron) {
		return inputConnectionsToWeight.get(inputNeuron);
	}

	@Override
	public void setWeight(Neuron inputNeuron, Float weight) {
		inputConnectionsToWeight.put(inputNeuron, weight);
	}

	@Override
	public void updateOutput() {
		Float propagation = strategy.propagation(inputConnectionsToWeight,
				biasValue);
		outputValue = strategy.activation(propagation);
	}

	@Override
	public void updateDelta(Float errorFactor) {
		deltaValue = strategy.findDelta(outputValue, errorFactor);
	}

	@Override
	public void updateWeight() {
        inputConnectionsToWeight.keySet().stream().forEach((inputNeuron) ->
        {
            Float newWeight = strategy.findNewWeight(getWeight(inputNeuron), inputNeuron.getOutput(), deltaValue);
            inputConnectionsToWeight.put(inputNeuron, newWeight);
        });
	}

	@Override
	public void updateBias() {
		biasValue = strategy.findNewBias(biasValue, deltaValue);
	}

	@Override
	public void setOutput(Float output) {
		this.outputValue = output;
	}

}
