package de.softwareforen.nn.base.network.strategy.functions;

import java.util.function.Function;

public interface BiasFunction extends Function<Float, Float> {

}
