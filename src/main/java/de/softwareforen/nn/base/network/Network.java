package de.softwareforen.nn.base.network;

import de.softwareforen.nn.base.network.neuron.Neuron;
import de.softwareforen.nn.base.network.training.TrainingData;
import de.softwareforen.nn.exceptions.InitialisationException;
import java.util.ArrayList;
import java.util.List;

public class Network {

	private Layer inputLayer;
	private final List<Layer> hiddenLayers;
	private Layer outputLayer;

	Network(int countHiddenLayers) {
		hiddenLayers = new ArrayList(countHiddenLayers);
	}

	public float train(List<TrainingData> ts) throws InitialisationException {
		float maxError = 0f;
		for (TrainingData t : ts) {
			maxError = Math.max(maxError, train(t));
		}
		return maxError;
	}

	public int getHiddenLayersCount() {
		return hiddenLayers.size();
	}

	public Layer getHiddenLayer(int index) {
		return hiddenLayers.get(index);
	}

	private float train(TrainingData t) throws InitialisationException {
		if (t.getInputs().size() != inputLayer.size()) {
			throw new InitialisationException(
					"Eingabedaten stimmen nicht mit der Anzahl der Eingabeneuronen zusammen.");
		}
		if (t.getOutputs().size() != outputLayer.size()) {
			throw new InitialisationException(
					"Ausgabedaten stimmen nicht mit der Anzahl der Ausgabeneuronen zusammen.");
		}
		calculate(t.getInputs());

		calculateDeltasForOutputLayer(t.getOutputs());

		calculateDeltasForHiddenLayers();

		calculateFreeParams();

		return calcError(t.getOutputs());

	}

	private void calculateFreeParams() {
		for (Layer hiddenLayer : hiddenLayers) {
			for (Neuron ne : hiddenLayer) {
				ne.updateBias();
				ne.updateWeight();
			}
		}
		for (Neuron ne : outputLayer) {
			ne.updateBias();
			ne.updateWeight();
		}
	}

	private void calculateDeltasForHiddenLayers() {
        for (int i = hiddenLayers.size()-1; i >= 0; i--) {
            Layer currentLayer = hiddenLayers.get(i);
            currentLayer.stream().forEach((neuron) -> {
                Float errorFactor = 0.0f;
                for (Neuron connectedNe : neuron.getOutputConnections()) {
                    errorFactor
                        += connectedNe.getDeltaValue() * connectedNe
                        .getWeight(neuron);
                }
                neuron.updateDelta(errorFactor);
            });
        }
    }

	private void calculateDeltasForOutputLayer(List<Float> expectedOutputs) {
		for (int i = 0; i < outputLayer.size(); i++) {
			Neuron outputNe = outputLayer.get(i);
			outputNe.updateDelta(expectedOutputs.get(i) - outputNe.getOutput());
		}
	}

	private void calculate(List<Float> inputs) {
		int i = 0;
		for (Neuron inputNe : inputLayer) {
			inputNe.setOutput(inputs.get(i));
			i++;
		}
		for (Layer hiddenLayer : hiddenLayers) {
			for (Neuron ne : hiddenLayer) {
				ne.updateOutput();
			}
		}
		for (Neuron outputNeuron : outputLayer) {
			outputNeuron.updateOutput();
		}
	}

	public List<Float> run(List<Float> inputs) {
		calculate(inputs);
		return getOutput();
	}

	private List<Float> getOutput() {
        List<Float> output = new ArrayList();
        outputLayer.stream().forEach((outputNe) -> {
            output.add(outputNe.getOutput());
        });
        return output;
    }

	public void addHiddenLayer(Layer layer) {
		hiddenLayers.add(layer);
	}

	public void setInputLayer(Layer inputLayer) {
		this.inputLayer = inputLayer;
	}

	public void setOutputLayer(Layer outputLayer) {
		this.outputLayer = outputLayer;
	}

	private float calcError(List<Float> expectedOutputs) {
		float maxError = 0f;
		for (int i = 0; i < expectedOutputs.size(); i++) {
			Float currentOutput = getOutput().get(i);
			Float expectedOutput = expectedOutputs.get(i);
			maxError = Math.max(maxError,
					Math.abs(expectedOutput - currentOutput));
		}
		return maxError;
	}

	public Layer getInputLayer() {
		return inputLayer;
	}

	public Layer getOutputLayer() {
		return outputLayer;
	}
}
