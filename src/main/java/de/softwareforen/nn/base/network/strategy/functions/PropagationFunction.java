package de.softwareforen.nn.base.network.strategy.functions;

import de.softwareforen.nn.base.network.neuron.Neuron;
import java.util.Map;
import java.util.function.Function;

public interface PropagationFunction extends
		Function<Map<Neuron, Float>, Float> {

}
