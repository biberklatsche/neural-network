package de.softwareforen.nn.base.network.strategy.functions;

import de.softwareforen.nn.base.network.training.Constants;

public class Bias {

	public static BiasFunction dynamicBias() {
		return (deltaValue) -> Constants.LEARNING_RATE * deltaValue;
	}
}
