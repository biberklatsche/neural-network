package de.softwareforen.nn.base.network.strategy;

import de.softwareforen.nn.exceptions.InitialisationException;

public class ActivationNotFoundException extends InitialisationException {

	public ActivationNotFoundException(String message) {
		super(message);
	}

}
