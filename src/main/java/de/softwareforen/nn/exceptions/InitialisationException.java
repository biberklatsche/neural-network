package de.softwareforen.nn.exceptions;

public class InitialisationException extends Exception {
	public InitialisationException(String message) {
		super(message);
	}
}
