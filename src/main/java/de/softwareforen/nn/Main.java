package de.softwareforen.nn;

import de.softwareforen.nn.base.network.Network;
import de.softwareforen.nn.base.network.NetworkFactory;
import de.softwareforen.nn.base.network.strategy.Strategy;
import de.softwareforen.nn.base.network.strategy.StrategyFactory;
import de.softwareforen.nn.base.network.strategy.functions.Activation;
import de.softwareforen.nn.base.network.strategy.functions.Bias;
import de.softwareforen.nn.base.network.strategy.functions.Propagation;
import de.softwareforen.nn.base.network.training.TrainingData;
import de.softwareforen.nn.exceptions.InitialisationException;
import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) throws InitialisationException {
		List<Float> inputs1 = Arrays.<Float> asList(0.0f, 0.0f);
		List<Float> outputs1 = Arrays.<Float> asList(0.0f);
		TrainingData data1 = new TrainingData(inputs1, outputs1);
		List<Float> inputs2 = Arrays.<Float> asList(1.0f, 0.0f);
		List<Float> outputs2 = Arrays.<Float> asList(0.0f);
		TrainingData data2 = new TrainingData(inputs2, outputs2);
		List<Float> inputs3 = Arrays.<Float> asList(0.0f, 1.0f);
		List<Float> outputs3 = Arrays.<Float> asList(0.0f);
		TrainingData data3 = new TrainingData(inputs3, outputs3);
		List<Float> inputs4 = Arrays.<Float> asList(1.0f, 1.0f);
		List<Float> outputs4 = Arrays.<Float> asList(1.0f);
		TrainingData data4 = new TrainingData(inputs4, outputs4);
		List<TrainingData> datas = Arrays.asList(data1, data2, data3, data4);

		Strategy strategy = StrategyFactory.create()
				.with(Activation.sigmoidDerivation())
				.with(Activation.sigmoidFunction()).with(Bias.dynamicBias())
				.with(Propagation.sumFunction()).build();
		Network network = NetworkFactory.create().with(2, 10, 10, 1)
				.with(strategy).build();
		float error;
		int counter = 0;
		do {
			error = network.train(datas);
			counter++;
			System.out.println("Iterationen: " + counter + " Error: " + error);
		} while (error > 0.2);

		List<Float> output1 = network.run(inputs1);
		System.out.println("Input: " + Arrays.toString(inputs1.toArray())
				+ " Output: " + Arrays.toString(output1.toArray()));
		List<Float> output2 = network.run(inputs2);
		System.out.println("Input: " + Arrays.toString(inputs2.toArray())
				+ " Output: " + Arrays.toString(output2.toArray()));
		List<Float> output3 = network.run(inputs3);
		System.out.println("Input: " + Arrays.toString(inputs3.toArray())
				+ " Output: " + Arrays.toString(output3.toArray()));
		List<Float> output4 = network.run(inputs4);
		System.out.println("Input: " + Arrays.toString(inputs4.toArray())
				+ " Output: " + Arrays.toString(output4.toArray()));
	}
}
